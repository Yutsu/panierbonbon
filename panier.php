<?php
session_start();
?>
<html>
<head>
  <title> Panier d'achats</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class ="container">
<h2>Votre Panier </h2>
        <table>
       <thead>
         <tr>
           <th class="text" >Produit</th>
            
           <th>Prix Unit.</th>
           <th>Quantité</th>
           <th>Montant</th>
            <th colspan='2'>Actions </th>
           </tr>
       </thead>
       <tbody>

 
	<?php
		if(!empty($_SESSION['succes'])){
	?>
	
	<div class ="success">
		<?php echo $_SESSION['succes'];?>
	</div>
	

 
          <?php 
		  }
	
			unset($_SESSION['succes']);
			require('fonction.php');
			$bdd = connection();
			
			$total = 0;
			$montant = 0;
			$tva = 0;
			$fdp = 0;
			$totalTTC = 0;
			
			if(isset($_SESSION['panier'])){
              foreach($_SESSION['panier'] as $id=>$quantite ){
					$sql = $bdd->prepare("select * from bonbon.produit where id=?");
					$sql->execute(array($id));
					

					while($bonbon=$sql->fetch(PDO::FETCH_OBJ)){
						echo "
								<tr>
									<td> " .$bonbon->nom ." </td>
									<td> " .$bonbon->prix . " € </td>
									<td> $quantite </td>
									<td> " .$bonbon->prix*$quantite. " € </td>
									<td> <a href ='ajout_panier1.php?id=" .$bonbon->id. "'><img src ='Images/ajouterPanier.jpg'></a></td>
									<td> <a href ='retirer_panier.php?id=" .$bonbon->id. "'><img src ='Images/retirerPanier.jpg'></a></td>
								</tr> ";
								
								$montant = $bonbon->prix*$quantite ;
								$total = $total + $montant ;
								
								$tva = ($total/100)*19.6;
								
								$fdp = 5;
								$totalTTC = $total + $tva + $fdp;
							
					
					}
				}
                  
			}
			
		   ?>
		</table>

		<div class ="total">
		     
			
				<p><span>Total HT</span> <?php echo "" .$total ." €" ;?>
				<span>TVA (19,6%)</span> <?php echo "". round($tva,2)." €"; ?> 
				<span>Frais de port</span> <?php echo "" .$fdp . " €"; ?> 
				<span>Total TTC</span> <?php echo "" . round($totalTTC,2). " € "; ?></p>

			<p><a href='index.php' ><button>Continuer mes achats </button>
			<a href='payer.php' ><button>Payer </button>
			<a href='vider.php' ><button>Vider le panier </button></p>
		</div>
	
	

		
</div>
</body>
</html>